/**
 * Project: ModTime
 */


#include "Time.h"

/**
 * Time implementation
 */


/**
 * @return void
 */
void Time::getTime() 
{
  cout << hour.getMethode() << " : "
       << minute.getMethode() << " : "
       << second.getMethode() << " Uhr" << endl;
}

/**
 * @return void
 */
void Time::reset() 
{
    second.reset();
    minute.reset();
    hour.reset();
}

/**
 * @return void
 */
void Time::startTime()
{
    second.modulo();
}


/**
 * Constructor
 */
Time::Time() : hour(24), minute(60, &hour), second(60, &minute)
{
//  ModCounter hour;
//  ModCounter minute(60, &hour);
//  ModCounter second(60, &minute);
}