/**
 * Project: ModTime
 */


#include "ModCounter.h"

/**
 * ModCounter implementation
 */


ModCounter::ModCounter() 
{
//  Counter();
}

/**
 * @param modValue
 * @param *pNext
 */
   ModCounter::ModCounter(int modValue, ModCounter* pNext) :modValue(modValue),pNext(pNext)
{
  //count = 0;
  //this->modValue = modValue;
  //this->pNext = pNext;

}


/**
 * @return void
 */
void ModCounter::modulo() 
{
  // Modulo operation
  count = (count + 1) % modValue;
  
  // Calling the next Counter
  if (!count && pNext)
    pNext->modulo();

}
