#include <iostream>
#include "Time.h"

using namespace std;

int main()
{
  // Constructor
  Time NewTime;
  
  cout << "Get new time value" << endl;
  
  for(int i = 0; i<=70; i++)
  {    
    // start Time
    NewTime.startTime();
  }
   
   // get Time value  
   NewTime.getTime();

  cout << "Resetting Time" << endl;
    
  // reset Time
  NewTime.reset();
  
  // get Time value
  NewTime.getTime();
    
  int test = (59 +5) % 60;
  cout << test << endl;
  
  return 0;
}
