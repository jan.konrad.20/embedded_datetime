/**
 * Project: ModTime
 */
#include "Counter.h"


/**
 * Returns the value of count
 * @return int
 */
int Counter::getMethode() 
{
    return count;
}

/**
 * Increments the variable count with 1
 * @return void
 */
void Counter::incCounter() 
{
    count++;
}

/**
 * Resetting the count value
 * @return void
 */
void Counter::reset() 
{
    count = 0;
}


/**
 * Constuctor of the instance Counter
 */
Counter::Counter(int startvalue):count(startvalue){}