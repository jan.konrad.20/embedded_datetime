/**
 * Project: ModTime
 */

#include "Counter.h"

#ifndef _MODCOUNTER_H
#define _MODCOUNTER_H

class ModCounter : public Counter {
ModCounter();  
public: 
    
    ModCounter(int modValue, ModCounter* pNext=nullptr);
    void modulo();
    
  private:
    int modValue;
    ModCounter* pNext;
};

#endif //_MODCOUNTER_H