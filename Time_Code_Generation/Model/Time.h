/**
 * Project: ModTime
 */

#include "ModCounter.h"
#include <iostream>

using namespace std;

#ifndef _TIME_H
#define _TIME_H

class Time {
  public: 
    void getTime();
    void reset();
    void startTime();
    Time();
    
  private: 
    ModCounter hour;
    ModCounter minute;
    ModCounter second;
};

#endif //_TIME_H