/**
 * Project: ModTime
 */


#ifndef _COUNTER_H
#define _COUNTER_H

class Counter {
  
  public: 
    int getMethode();
    void incCounter();
    void reset();
    Counter(int startvalue = 0);
    
  protected: 
    int count;
};

#endif //_COUNTER_H