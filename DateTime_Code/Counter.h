/**
 * @file Counter.h
 * @brief header of the counter class
 * @author Jana Konrad
 * @author Laura Kilthau (doxygen documentation)
 */


#ifndef _COUNTER_H
#define _COUNTER_H

/**
 * @class Counter
 * @brief class for counting and resetting the counter
 */
class Counter {
   
   protected: 
    /**
     * @brief default constructor
     */
    Counter* pNext = nullptr; /// pointer to the next counter
  
   public: 
    int count; /// counter

    /**
     * @brief returns the counter
     * @return counter 
     */
    int getMethode();
    
    /**
     * @brief virtual method or counting
     * @return void
     */
    void virtual increment();
    
    /**
     * @brief resetting of the counter
     * @return void
     */
    void reset();
    
    /**
     * @brief constructor with start value and pointer to the next counter as parameters
     * @param startvalue start value
     * @param pNext pointer to the next counter
     */
    Counter(int startvalue = 0, Counter* pNext = nullptr);
    
    
};

#endif //_COUNTER_H