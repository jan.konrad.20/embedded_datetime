/**
 * @file HMI.cpp
 * @brief implementation of the HMI class
 * @author Lena Heller
 * @author Laura Kilthau (doxygen documentation)
 */

#include <iostream>
#include <vector>
#include <sstream>
#include <string>
#include <algorithm>
#include <iterator>
#include <bitset>

#include "HMI.h"
#include "Date.h"

HMI::HMI() :
  physical(),
  date(),
  abbruch(false)
{
  cout<< endl << " DHBW Mosbach ";
  cout<< endl << " Embedded Systeme ";
  cout<< endl << " Kalender "<< endl;
  
  // decision: physical or virtual
    
  Date date;
  Physical physical;
}

void HMI::start(int startDate[6])
// array Date:
// [year|month|day|hour|minute|second]
{
  // set start time
  date.setStart(startDate);
  
  while (stop() == false)
  {    
    // start method
    //-> counts seconds and checks day and leap year
    date.start();
    
    // for the output of the current value in the loop
    cout << date.getDateTime() << endl;
  }
}

string HMI::printDateTime()
{
  cout << "date: " << date.getDateTime() << endl;
  return date.getDateTime();
}

bool HMI::stop()
{
  buttonState = physical.getButtonState();
  return buttonState[1];  // button D4
}

bool HMI::startButton()
{
  buttonState = physical.getButtonState();
  return buttonState[0];  // button D3
}

void HMI::setLED(bool activation, int nbr)
{
  physical.LED(activation, nbr);
}

int* HMI::decToBinary(int n)
{
  static int binaryNum[3];
  int i = 0;
  while (n > 0) 
  {
      binaryNum[i] = n % 2;
      n = n / 2;
      i++;
  }
  return binaryNum;
}



    