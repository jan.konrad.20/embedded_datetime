/**
 * @file Counter.cpp
 * @brief implementation of the counter class
 * @author Jana Konrad
 * @author Laura Kilthau (doxygen documentation)
 */


#include "Counter.h"

Counter::Counter(int startvalue, Counter* pNext) : count(startvalue), pNext(pNext) 
{ 

}

int Counter::getMethode() 
{
    return count;
}

void Counter::increment()
{
    count++;

}

void Counter::reset() 
{
    count = 0;
}