/**
 * @file StartStopCounter.h
 * @brief header of the start stop counter class
 * @author Jana Konrad
 * @author Laura Kilthau (doxygen documentation)
 */


#ifndef _STARTSTOPCOUNTER_H
#define _STARTSTOPCOUNTER_H

#include "Counter.h"
#include "ModCounter.h"
#include "Counter.h"

/**
 * @class StartStopCounter
 * @brief class for counting with a start and stop value
 *        virtual inheritance of the class Counter
 */
class StartStopCounter: public virtual Counter {
  
    private: 
        int startValue = 1;     /// start value, 1 per default

    public:
        int stopValue = 12;     /// stop value, 12 per default


        /**
         * @brief constructor with start value and a pointer to the next counter as parameters
         *        Transfers the start value with the constructor to the count of the class Counter.
         *        Passes the pointer to the next counter using the constructor of the cass Counter.
         * @param startvalue start value for the start stop counter
         * @param pNext pointer to the next counter
         */  
        StartStopCounter(int startValue, Counter* pNext);
        
        /**
         * @brief virtual method for counting
         *        Advances the counter via pNext to the corresponding increment method when count exceeds the stop value and pNext does not point to the nullptr.
         * @return void 
         */             
        void virtual increment();
};

#endif //_STARTSTOPCOUNTER_H