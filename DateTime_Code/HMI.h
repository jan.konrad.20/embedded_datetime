/**
 * @file HMI.h
 * @brief header of the HMI class
 * @author Lena Heller
 * @author Laura Kilthau (doxygen documentation)
 */

#include <string>
#include <vector>

#include <iostream>
#include "Date.h"
#include "Physical.h"
   
using namespace std;

#ifndef _HMI_H
#define _HMI_H

/**
 * @class HMI
 * @brief class for the human interface
 */
class HMI {
  
    private:
      Date date;                /// instance of the class Date
      Physical physical;        /// instance of the class Physikal
      bool* buttonState;        /// button state  
      int binaer[10];           /// array for the binary display

      /**
       * @brief checks if button to stop is pressed
       * @return button state
       */
      bool stop();


    public: 
       /**
        * @brief default constructor
        */   
      HMI();
       
      /**
       * @brief runs counter
       * @param startDate startvalue for the date
       * @return void
       */
      void start(int startDate[6]);
      
      /**
       * @brief prints the date and time
       * @return string of date and time
       */
      string printDateTime();
      
      bool abbruch;     /// boolean for interrupt
      
      /**
       * @brief method for converting decimal to binary
       * @param n decimal value to convert into binary 
       * @return binary number
       */
      int* decToBinary(int n);
      
      //void show();
      //bool anzeige();
      
      //int** convertToBinary(const vector<int>& digits);

      /**
       * @brief checks if button for starting is pressed
       * @return button state
       */
      bool startButton();  
      
      /**
       * @brief sets LEDs for display
       * @return void
       */
      void setLED(bool activation, int nbr);
      
};

#endif //_HMI_H