/**
 * @file ModCounter.cpp
 * @brief implementation of the modulo counter class
 * @author Jana Konrad
 * @author Laura Kilthau (doxygen documentation)
 */

#include "ModCounter.h"

ModCounter::ModCounter(int startvalue, int modValue, Counter* pNext) : modValue(modValue), Counter(startvalue, pNext)
{ 
  
}

void ModCounter::increment() 
{
  // modulo operation
  count = (count + 1) % modValue;
   
  if (!count && pNext)
  {
    pNext-> increment();
  }
}
