/**
 * @file Virtual.h
 * @brief header of the virtual class
 * @author Lena Heller
 * @author Laura Kilthau (doxygen documentation)
 */

#include <iostream>

using namespace std;


#ifndef _VIRTUAL_H
#define _VIRTUAL_H

/**
 * @class Virtual
 * @brief class for the virtual interface
 */
class Virtual {
  
    private:
      // test

    public: 
      /**
       * @brief default constructor
       */   
       Virtual();
       
      /**
       * @brief virtual menu for selecting the display options
       * @return void
       */
       void showData();
};

#endif //_VIRTUAL_H