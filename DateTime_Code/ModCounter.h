/**
 * @file ModCounter.h
 * @brief header of the modulo counter class
 * @author Jana Konrad
 * @author Laura Kilthau (doxygen documentation)
 */


#ifndef _MODCOUNTER_H
#define _MODCOUNTER_H

#include "Counter.h"

/**
 * @class ModCounter
 * @brief class for counting with a modulo
 *        virtual inheritance of the class Counter
 */
class ModCounter: public virtual Counter {

public: 
    /**
     * @brief constructor with start value, modulo value and a pointer to the next counter as parameters
     *        Transfers the start value with the constructor to the count of the class Counter.
     *        Passes the pointer to the next counter using the constructor of the cass Counter.
     * @param startvalue start value for the modulo counter
     * @param modValue value of the modulo
     * @param pNext pointer to the next counter
     */    
    ModCounter(int startvalue, int modValue, Counter* pNext);
    
    /**
     * @brief virtual method for counting
     *        Passing the counter via pNext to the corresponding increment method
     * @return void 
     */     
    void virtual increment();
    
    
  private:
    int modValue;       /// modulo value

    int startValue = 0; /// start value for the modulo counter, zero by default
   

};

#endif //_MODCOUNTER_H