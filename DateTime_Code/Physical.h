/**
 * @file Physical.h
 * @brief header of the Physical class
 * @author Lena Heller
 * @author Laura Kilthau (doxygen documentation)
 */

#include "stm_gpio.h"

#ifndef _PHYSICAL_H
#define _PHYSICAL_H

/**
 * @class Physical
 * @brief class for the physical interface
 */
class Physical {
  public: 
   /**
    * @brief default constructor
    */   
    Physical();
      
    /**
     * @brief activates the LEDs
     * @param activation if true, LEDs schould be activated
     * @param nbr defines which LED should be activated/deactivated
     * @return void
     */
    void LED(bool activation, int nbr);
    
    /**
     * @brief gets the button state
     * @return button state
     */
    bool* getButtonState();
    
  private: 
    bool buttonState[2];        /// button state
    StmGpio PortA;              /// instance of the class StmGpio
    StmGpio PortB;              /// instance of the class StmGpio
    bool prev1;                 /// variable used for rising edge detection
    bool prev2;                 /// variable used for rising edge detection
    bool act1;                  /// variable used for rising edge detection      
    bool act2;                  /// variable used for rising edge detection
};

#endif //_PHYSICAL_H