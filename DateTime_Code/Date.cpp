/**
 * @file Date.cpp
 * @brief implementation of the date class
 * @author Jana Konrad
 * @author Laura Kilthau (doxygen documentation)
 */

#include "Date.h"

Date::Date() : 
  year(1999, nullptr), 
  month(2, &year), 
  day(30, &month), 
  hour(0, 24, &day), 
  minute(0, 60, &hour), 
  second(0, 60, &minute)
{}

Date::Date(int start[6]): 
  year(start[0], nullptr), 
  month(start[1], &year), 
  day(start[2], &month), 
  hour(start[3], 24, &day), 
  minute(start[4], 60, &hour), 
  second(start[5], 60, &minute)
{}

void Date::setStart(int startDateTime[6])
{
  year.count = startDateTime[0];
  month.count = startDateTime[1];
  day.count = startDateTime[2];
  hour.count = startDateTime[3];
  minute.count = startDateTime[4];
  second.count = startDateTime[5];
  // [year|month|day|hour|minute|second]
}

string Date::getDate() 
{
  string date = to_string(day.getMethode()) + " . " + to_string(month.getMethode()) + " . " + to_string(year.getMethode());
  return (date);
}


string Date::getTime() 
{
  string time = to_string(hour.getMethode()) + " : " + to_string(minute.getMethode()) + " : " + to_string(second.getMethode()) + " Uhr";
  return (time);
}


string Date::getDateTime() 
{
  string date = to_string(day.getMethode()) + " . " + to_string(month.getMethode()) + " . " + to_string(year.getMethode()) ;
  string time = to_string(hour.getMethode()) + " : " + to_string(minute.getMethode()) + " : " + to_string(second.getMethode()) + " Uhr";
  return (date + " - " + time);
}


void Date::reset() 
{
    second.reset();
    minute.reset();
    hour.reset();
    day.reset();
    month.reset();
    year.reset();
}


bool Date::leapYear() 
{  
  if( (year.count % 100) == 0 && (year.count % 400) != 0 )
  {
    return false; // no leap year
  }
  else if( (year.count % 4) == 0  || (year.count % 400) == 0)
  {
    return true; // leap year
  }
  return false; // no leap year
}


void Date::start()
{
  // loop for determining the stop value for the days in each month
  // iteration up to the value 13 (corresponds to the array numberOfDays)
  for (int i = 0; i <= 12; i++)
  {
    // checking whether the month corresponds to the loop variable
    if (month.count == i)
    {
      // checking whether it is the second month (february) and whether it is a leap year
      if(month.count == 2 && leapYear())
      {
        // set i = 0 when it is a leap year -> stop value is 29
        i = 0;
      }
      day.stopValue = numberOfDays[i]; // assignment of the array value in numberOfDays at the position i to the stop value of the days
      break;
    }
  }
    
  second.increment();

}

void Date::startStop(bool* stop)
{
  while(*stop == false)
  {
    start();
  }
}
