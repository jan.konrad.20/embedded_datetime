/**
 * @file Virtual.cpp
 * @brief implementation of the Virtual class
 * @author Lena Heller
 * @author Laura Kilthau (doxygen documentation)
 */

#include "Virtual.h"

Virtual::Virtual() 
{
    cout << endl << " DHBW Mosbach ";
    cout << endl << " Embedded Systeme ";
    cout << endl << " Kalender - virtual"<< endl;
}

void Virtual::showData() 
{
   int keyentry; // input from the menu
   string input;
   string newdate;
   
   system("cls");  
   cout << "\n\n\n\tHauptmenu";
   cout << "\n\t 1. Start (s, S oder start)";
   cout << "\n\t 2. Stop (s, S oder stop)";
   cout << "\n\t 3. Eingabe Startzeitpunkt (ein)";
   cout << "\n\t 4. Ausgabe (aus)";
   cout << "\n\t 5. Abbruch";
   cout << "\n\n\tBitte waehlen sie 'start', 'stop', ein' oder 'aus' ";
   
   do
   {
      cin >> input;
      system("cls");  // clear display
      
      if ((input=="S" and keyentry!=1) or (input=="s" and keyentry!=1) or input=="start")
      {
        keyentry = 1;
      }
      else if ((input=="S" and keyentry==1) or (input=="s" and keyentry==1) or input=="stop")
      {
        keyentry = 2;
      }
      else if (input == "ein")
      {
        keyentry = 3;
      }
      else if (input == "aus")
      {
        keyentry = 4;
      }
      else
      {
        keyentry = 0;
      }

      switch(keyentry)  // query for the desired function
      {
        case 1:
            cout <<"\n\t START" << keyentry << endl;
            // cout <<"\n\n\t" ; // Date.getDate();
            // cout <<"\n\n\tAktuelles Datum: " << endl;
            //cout <<"\n\n\t" ; // Date.getDate();
            break;
        case 2:
            cout <<"\n\t STOP" << endl; 
            // cout <<"\n\n\t" ; // Date.getTime();
            // cout <<"\n\n\tAktuelle Uhrzeit: " << endl; 
            // cout <<"\n\n\t" ; // Date.getTime();
            break;
        case 3:
             cout <<"\n\t Eingabe Datum und Uhrzeit" << endl; 
             cin >> newdate;
             // cout <<"\n\n\tAktuelles Datum und Uhrzeit: " << endl; 
             // cout <<"\n\n\t" ; // Date.getDateTime();
             break;
        case 4:
             cout <<"\n\t Datum und Uhrzeit: "; 
             // cin >> newdate; // input of the array for a new time and date
             break;
        case 0:
            cout <<"\n\n\t... Und Tschuesss!";
            break;
        default :
            cout <<"tets";
       }
       // cin.ignore();
       cin.get();  // type enter to continue
   }
   while(keyentry!=0);

}