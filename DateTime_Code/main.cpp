/** @file main.cpp
 * @brief main function for the project "Kalender" in embedded systems
 * @author Jana Konrad
 * @author Lena Heller
 * @author Laura Kilthau (doxygen documentation)
 */

#include <iostream>
#include <string>
#include <bitset>
#include <vector>

#include "Date.h"
#include "Virtual.h"
#include <iostream>
#include "HMI.h"

using namespace std;

int main()
{
    HMI hmi;
//    string input = "30 . 02 . 1999 - 0 : 0 : 0 Uhr";
//    int* numbers = hmi.splitNumbers(input);
        
    while (true)
    {
      if (hmi.startButton() == false) 
      {
        // turn of LEDs 
        hmi.setLED(false, 0);
        hmi.setLED(false, 1);
        hmi.setLED(false, 2);
        hmi.setLED(false, 3);
      }
      else
      {
        // get user input for start date 
        int startArray[6] = {0, 0, 0, 0, 0, 0};
        cout << "Bitte Startdatum Eingeben (Format:YYYY MM DD HH MM SS): " << endl;
        cin >> startArray[0] >> startArray[1] >> startArray[2] >> startArray[3] >> startArray[4] >> startArray[5];
        // display the user input
        for (int i = 0; i <= 5; i++) {
            cout << startArray[i] << " ";
        }
        cout << endl;
        
        // turn on LEDs 
        hmi.setLED(true, 0);
        hmi.setLED(true, 1);
        hmi.setLED(true, 2);
        hmi.setLED(true, 3);
        hmi.start(startArray);
        hmi.printDateTime();
      } 
    
    }
    
  
  return 0;
}