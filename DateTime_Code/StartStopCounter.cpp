/**
 * @file StartStopCounter.cpp
 * @brief implementation of the start stop counter class
 * @author Jana Konrad
 * @author Laura Kilthau (doxygen documentation)
 */

#include "StartStopCounter.h"

StartStopCounter::StartStopCounter(int startValue, Counter* pNext) : Counter(startValue,pNext)
{

}

void StartStopCounter::increment() 
{
     // counter always plus 1
     count++;
     
     if(count > stopValue && pNext)
     {
       count = startValue;
       pNext-> increment();
     }
}
