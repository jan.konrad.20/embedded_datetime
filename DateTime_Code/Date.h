/**
 * @file Date.h
 * @brief header of the date class
 * @author Jana Konrad
 * @author Laura Kilthau (doxygen documentation)
 */

#include "ModCounter.h"
#include "StartStopCounter.h"
#include <iostream>

using namespace std;

#ifndef _DATE_H
#define _DATE_H

/**
 * @class Date
 * @brief class for getting the date and time correctly
 */
class Date {

    public: 
        /**
         * @brief sets a starting point for the date/counter
         * @return void
         */
        void setStart(int startDateTime[6]);      
      
        /**
         * @brief returns the date
         * @return date 
         */
        string getDate();
        
        /**
         * @brief returns the time
         * @return time 
         */
        string getTime();
        
        /**
         * @brief returns the date and time
         * @return string of date and time
         */
        string getDateTime();
        
        /**
         * @brief starts the counter for seconds and checks for leap year and the number of days in the month
         * @return void
         */
        void start();
        
        /**
         * @brief resets the date and the time calling the individual reset methods of the attributes
         * @return void 
         */        
        void reset();
        
        /**
         * @brief method for returning if there is a leap year or not
         * @return true if there is a leap year 
         */        
        bool leapYear();
        
        /**
         * @brief default constructor
         *        Declares the individual attributes with default values via the assignment list.
         */        
        Date();
        
        /**
         * @brief constructor with start values as parameter
         *        Declares the individual attributes via the assignment list.
         * @param start start values for date and time
         */        
        Date(int start[6]);
        
        /**
         * @brief  clarifies if counter should stop or not
         * @return void 
         */
        void startStop(bool* stop);
        
        StartStopCounter year;          /// year attribute of the type StartStopCounter
        StartStopCounter month;         /// month attribute of the type StartStopCounter
        StartStopCounter day;           /// day attribute of the type StartStopCounter
        ModCounter hour;                /// hour attribute of the type ModCounter
        ModCounter minute;              /// minute attribute of the type ModCounter
        ModCounter second;              /// second attribute of the type ModCounter
        

    private: 
        bool* stop;                     /// if true, counter should stop        
        int numberOfDays[13] = {29, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};    /// array with the number of days in each month
};

#endif //_DATE_H