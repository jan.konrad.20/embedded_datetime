/**
 * @file Physical.cpp
 * @brief implementation of the Physical class
 * @author Lena Heller
 * @author Laura Kilthau (doxygen documentation)
 */

#include <iostream>
#include "stm_gpio.h"
#include "Physical.h"

using namespace std;

#define LED0 8     // PA8 -> D7
#define LED1 9     // PA9 -> D8
#define LED2 10    // PA10 -> D6
#define LED3 10    // PB10 -> D2
#define button1 3  // PB3 -> D3
#define button2 5  // PB5 -> D4 (stop)

// Constructor
Physical::Physical() : 
  PortA(0x40020000), 
  PortB(0x40020400),
  prev1(false),
  prev2(false)  
{
  // set as outputs
  PortA.SetStandardOutPut(LED0);
  PortA.SetStandardOutPut(LED1);
  PortA.SetStandardOutPut(LED2);
  PortB.SetStandardOutPut(LED3);
  
  // set as inputs
  PortB.SetStandardInPut(button1);
  PortB.SetStandardInPut(button2);
}

void Physical::LED(bool activation, int nbr) 
{  
  if (activation == 1)
  {
    switch (nbr)
    {
      case 0:
        PortA.SetPin(LED0); 
        break;
      case 1:
        PortA.SetPin(LED1); 
        break;
      case 2:
        PortA.SetPin(LED2);
        break;
      case 3:
        PortB.SetPin(LED3);
        break;
    }
  }
  else
  {
    switch (nbr)
    {
      case 0:
        PortA.ClearPin(LED0); 
        break;
      case 1:
        PortA.ClearPin(LED1); 
        break;
      case 2:
        PortA.ClearPin(LED2);
        break;
      case 3:
        PortB.ClearPin(LED3);
        break;
    }
  }
}

bool* Physical::getButtonState() 
{  
  act1 = PortB.ReadPin(button1);
  act2 = PortB.ReadPin(button2);
  // cout << "1: " << act1 << " 2: " << act2 << endl;
  
  // rising edge detection
  if (act1==1 and prev1==0)
    buttonState[0] = 1;
  else 
    buttonState[0] = 0;
  if (act2==1 and prev2==0)
    buttonState[1] = 1;
  else 
    buttonState[1] = 0;
  
  prev1 = act1;
  prev2 = act2;
  
  return buttonState;
}

